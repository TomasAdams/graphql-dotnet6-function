using System;
using static HotChocolate.AspNetCore.ErrorCodes;

namespace HotChocolate.AspNetCore;

public class Errors
{
    public static IError NotFoundEx(int id)
    {
        return ErrorBuilder.New()
                .SetMessage($"Object not found with id {id}")
                .SetCode(NotFound)
                .Build();
    }
}