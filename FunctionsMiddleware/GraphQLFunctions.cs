using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HotChocolate.AspNetCore.Serialization;
using HotChocolate.Execution;
using HotChocolate.Language;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HotChocolate.AspNetCore
{
    public class GraphQLFunctions : IGraphQLFunctions
    {
        private readonly IHttpRequestParser _requestParser;
        private readonly IHttpResultSerializer _resultSerializer;
        private readonly IRequestExecutorResolver _requestExecutorResolver;

        public GraphQLFunctions(
            IHttpRequestParser requestParser,
            IHttpResultSerializer resultSerializer,
            IRequestExecutorResolver requestExecutorResolver)
        {
            _requestParser = requestParser;
            _resultSerializer = resultSerializer;
            _requestExecutorResolver = requestExecutorResolver;
        }

        public async Task<IActionResult> ExecuteFunctionsQueryAsync(HttpContext httpContext)
        {
            IExecutionResult result = default;
            try
            {
                using var stream = httpContext.Request.Body;

                var requestQuery = await _requestParser
                    .ReadJsonRequestAsync(stream, CancellationToken.None)
                    .ConfigureAwait(false);

                var executor = await _requestExecutorResolver.GetRequestExecutorAsync().ConfigureAwait(false);
                var queryRequest = requestQuery.Select(r => QueryRequestBuilder.From(r).Create()).FirstOrDefault();
                result = await executor.ExecuteAsync(queryRequest).ConfigureAwait(false);
            }
            catch (GraphQLRequestException ex)
            {
                result = QueryResultBuilder.CreateError(ex.Errors);
            }
            catch (Exception ex)
            {
                result = QueryResultBuilder.CreateError(new Error(ex.Message));
            }

            return await PrepareResponse(result);
        }

        private async Task<IActionResult> PrepareResponse(IExecutionResult queryResult)
        {
            var statusCode = _resultSerializer.GetStatusCode(queryResult);

            var t = await SerializeResult(queryResult);
            Console.WriteLine(statusCode);

            var result = new ObjectResult(t);
            result.StatusCode = ((int)statusCode);
            return result;
        }

        private static async Task<string> SerializeResult(IExecutionResult queryResult)
        {
            if (queryResult is IQueryResult payload)
            {
                    var json = await payload.ToJsonAsync().ConfigureAwait(false);

                return json;
            }

            return await queryResult.ToJsonAsync().ConfigureAwait(false);
        }
    }
}