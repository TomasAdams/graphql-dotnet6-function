using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace HotChocolate.AspNetCore
{
    public interface IGraphQLFunctions
    {
        Task<IActionResult> ExecuteFunctionsQueryAsync(HttpContext httpContext);
    }
}