namespace HotChocolate.AspNetCore;

public class ErrorFilter : IErrorFilter
{
    public IError OnError(IError error)
    {
        return ErrorBuilder.New()
                .SetMessage(error.Message)
                .SetCode(error.Code)
                .Build();
    }
}