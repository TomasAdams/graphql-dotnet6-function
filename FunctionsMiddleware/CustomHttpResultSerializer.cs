using System;
using System.Linq;
using System.Net;
using HotChocolate.AspNetCore.Serialization;
using HotChocolate.Execution;
using static HotChocolate.AspNetCore.ErrorCodes;

namespace HotChocolate.AspNetCore;

public class CustomHttpResultSerializer : DefaultHttpResultSerializer
{
    public override HttpStatusCode GetStatusCode(IExecutionResult result)
    {
        if (result is IQueryResult queryResult && queryResult.Errors?.Count > 0)
        {
            if (queryResult.Errors.Any(error => error.Code == NotFound))
            {
                return HttpStatusCode.NotFound;
            }
        }

        return base.GetStatusCode(result);
    }
}