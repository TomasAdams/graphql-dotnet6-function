using Microsoft.Extensions.DependencyInjection;

namespace HotChocolate.AspNetCore
{
    public static class AzureFunctionsMiddlewareExtension
    {
        public static IServiceCollection AddAzureFunctionsGraphQL(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IGraphQLFunctions, GraphQLFunctions>();

            return serviceCollection;
        }
    }
}