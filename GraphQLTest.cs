using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using HotChocolate.AspNetCore;

namespace Test1
{
    public class GraphQLTest
    {
        private readonly IGraphQLFunctions _graphQLFunctions;

        public GraphQLTest(IGraphQLFunctions graphQLFunctions)
        {
            _graphQLFunctions = graphQLFunctions;
        }
        [FunctionName("GraphQLTest")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            return await _graphQLFunctions.ExecuteFunctionsQueryAsync(req.HttpContext);
        }
    }
}
