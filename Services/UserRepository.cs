using System;
using System.Collections.Generic;
using System.Linq;
using HotChocolate;
using static HotChocolate.AspNetCore.Errors;
using Test1.Models;

namespace Test1.Services;
    public interface IUserRepository
    {
        User GetProfile(int id);
        User AddUser(User user);
        User Admin(int id, bool admin);
        ICollection<User> GetProfiles();
    }

    public class UserRepository : IUserRepository
    {
        public User GetProfile(int id)
        {
            var user = GetProfiles().Where(i => i.Id == id).FirstOrDefault();

            if(user == null)
                throw new GraphQLException(NotFoundEx(id));

            return user;
        }
        public User AddUser(User user)
        {
            Console.WriteLine("AddUser");
            return user;
        }

        public User Admin(int id, bool admin)
        {
            var user = GetProfile(id);
            user.IsAdmin = admin;

            return user;
        }

        public ICollection<User> GetProfiles()
        {
            return new List<User>()
            {
                new User()
                {
                    Id = 1,
                    Title = "Mr",
                    LastName = "Chand",
                    FirstName = "Abdul Salam"
                },
                new User()
                {
                    Id = 2,
                    Title = "Mr",
                    LastName = "Shaik",
                    FirstName = "Mohammed Umar"
                },
                new User()
                {
                    Id = 3,
                    Title = "Mr",
                    LastName = "Shaikh",
                    FirstName = "Faraaz"
                },
                new User()
                {
                    Id = 4,
                    Title = "Mr",
                    LastName = "Syed",
                    FirstName = "Nawaz"
                }
            };
        }
    }