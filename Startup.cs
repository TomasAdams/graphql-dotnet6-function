using HotChocolate;
using HotChocolate.AspNetCore;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Test1.Services;
using Test1.Types;

[assembly: FunctionsStartup(typeof(Test1.Startup))]

namespace Test1
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddSingleton<Query>();
            builder.Services.AddTransient<IUserRepository, UserRepository>();

            builder.Services.AddGraphQLServer()
            .AddQueryType<Query>()
            .AddFiltering()
            .AddSorting()
            .AddMutationType<Mutation>()
            .AddErrorFilter<ErrorFilter>();

            builder.Services.AddHttpResultSerializer<CustomHttpResultSerializer>();

            
            builder.Services.AddAzureFunctionsGraphQL();
        }
    }
}