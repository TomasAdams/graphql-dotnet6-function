using System.Collections.Generic;
using HotChocolate;
using HotChocolate.Data;
using Test1.Models;
using Test1.Services;

namespace Test1.Types
{
    public class Query
    {
        public User GetProfile([Service]IUserRepository repository, int id) => repository.GetProfile(id);

        [UseFiltering]
        [UseSorting]
        public ICollection<User> GetProfiles([Service] IUserRepository repository) => repository.GetProfiles();
    }
}