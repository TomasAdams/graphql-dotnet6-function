using HotChocolate;
using Test1.Models;
using Test1.Services;

namespace Test1.Types;

public class Mutation
{
    public User AddUser([Service]IUserRepository repository, int id, string firstName)
    {
        var user = new User()
        {
            Id = id,
            FirstName = firstName
        };
        return repository.AddUser(user);
    }

    public User UpdateUser([Service]IUserRepository repository, int id, bool admin)
    {
        return repository.Admin(id, admin);
    }
}